function onOpen() {
  var ui = DocumentApp.getUi();
  ui.createAddonMenu()
    .addItem('RAM Export', 'exportRam')
    .addToUi();
}

function exportRam() {
  var ui = DocumentApp.getUi();

  var selection = DocumentApp.getActiveDocument().getSelection();
  if (!selection) {
    ui.alert('Du musst alle Zellen auswählen, die das Johnny-Programm darstellen. Das heißt, den Inhalt der beiden Spalten (Hi und Lo) ohne Überschrift.');
    return;
  }

  var rangeElements = selection.getRangeElements();
  var fileContent = '';
  var highCol = true;
  for (var i = 0; i < rangeElements.length; i++) {
    var element = rangeElements[i].getElement();
    if (element.getType() != DocumentApp.ElementType.TABLE_CELL) {
      ui.alert('Bitte wähle nur (ganze) Tabellenzellen aus.');
      return;
    }

    var tableCell = element.asTableCell();
    var cellText = tableCell.getText().trim();

    if (!/^\d*$/.test(cellText)) {
      ui.alert('Die ausgewählten Tabellenzellen dürfen nur Zahlen enthalten.');
      return;
    }

    var value = parseInt(cellText, 10);

    if (highCol) { // Hi value (operation)
      if (value < 0 || value > 99) {
        ui.alert('Die Zahlen in den ausgewählten Zellen sind ungültig (erlaubt: 00 - 99).');
        return;
      }

      fileContent += value;
    } else { // Lo value (address)
      if (value < 0 || value > 999) {
        ui.alert('Die Zahlen in den ausgewählten Zellen sind ungültig (erlaubt: 000 - 999).');
        return;
      }

      // add leading zeros if necessary (length = 3)
      var value_s = String(value);
      while (value_s.length < 3) {
        value_s = "0" + value_s;
      }

      fileContent += value_s + '\n';
    }

    // switch which value is worked with (Hi or Lo)
    highCol = !highCol;
  }

  if (!highCol) { // odd amount of values
    ui.alert('Du musst zwei Spalten auswählen.');
    return;
  }

  // fill up to 1000 empty lines with zeros
  var lineAmount = rangeElements.length / 2;
  for (var i = 0; i < 1000 - lineAmount; i++) {
    fileContent += '0\n';
  }

  var response = ui.prompt('Gib einen Dateinamen an, der auf .ram endet (z. B. Übung1.ram)');
  if (response.getSelectedButton() != ui.Button.OK) {
    ui.alert('Du musst einen Dateinamen angeben.');
    return;
  }

  var fileName = response.getResponseText();
  if (!/.{1,}\.ram$/.test(fileName)) {
    ui.alert('Dieser Dateiname ist ungültig.');
    return;
  }

  var file = DriveApp.createFile(fileName, fileContent);
  var url = file.getDownloadUrl();

  var html = '<html><body><a href="' + url + '" target="_blank">Download</a></body></html>';
  var htmlOutput = HtmlService.createHtmlOutput(html)
  ui.showModelessDialog(htmlOutput, 'Lade Datei..');
}
